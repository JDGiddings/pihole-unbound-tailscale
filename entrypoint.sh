#!/bin/bash -e

exec /usr/sbin/tailscaled --tun=userspace-networking --socks5-server=localhost:1055 --state=/tailscale/tailscaled.state &
tailscale up --authkey=${AUTHKEY} --accept-dns=false --hostname=${HOSTNAME} --advertise-tags=${TAGS}
sleep 10

export ALL_PROXY=socks5://localhost:1055/
export VIRTUAL_HOST="${HOSTNAME}.${TS_DOMAIN}"
export ServerIP="$(tailscale ip --4)"

/etc/init.d/unbound start

mkdir -p /etc/ts-ssl/
tailscale cert --cert-file /etc/ts-ssl/cert.crt --key-file /etc/ts-ssl/key.key ${VIRTUAL_HOST}
cat /etc/ts-ssl/key.key /etc/ts-ssl/cert.crt > /etc/ts-ssl/ts.pem

/s6-init