FROM pihole/pihole:2021.10.1

ENV PIHOLE_DNS_="127.0.0.1#5335"
ENV DNSMASQ_LISTENING="all"
ENV DNSSEC=true

RUN apt-get update && apt-get install -y \
    curl \
    gnupg2 \
    unbound \
    && rm -rf /var/lib/apt/lists/*

RUN curl -fsSL https://pkgs.tailscale.com/stable/debian/buster.gpg | apt-key add - \
 && curl -fsSL https://pkgs.tailscale.com/stable/debian/buster.list | tee /etc/apt/sources.list.d/tailscale.list \
 && apt-get update && apt-get install -y \
    tailscale \
    && rm -rf /var/lib/apt/lists/*

COPY unbound-pihole.conf /etc/unbound/unbound.conf.d/pi-hole.conf
COPY lighttpd-external.conf /etc/lighttpd/external.conf 
COPY entrypoint.sh entrypoint.sh

RUN chmod +x entrypoint.sh
ENTRYPOINT ./entrypoint.sh